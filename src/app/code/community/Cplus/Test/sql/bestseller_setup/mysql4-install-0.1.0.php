<?php

$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'bestseller', array(
    'group'             => 'General',
    'type'              => 'int',
    'attribute_set'     => 'Default',
    'backend'           => '',
    'frontend'          => '',
    'label'             => 'Bestseller',
    'input'             => 'boolean',
    'class'             => '',
    'source'            => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => '0',
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,virtual,bundle,downloadable',
    'is_configurable'   => false
));
$installer->endSetup();