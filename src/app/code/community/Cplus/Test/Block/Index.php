<?php   
class Cplus_Test_Block_Index extends Mage_Catalog_Block_Product_List{   


    public function getBestsellerCollection() {

        $_storeID = Mage::app()->getStore()->getId();

        $_collections = Mage::getModel( 'catalog/product' )
            ->getCollection()
            ->addAttributeToSelect( '*' )
            ->addAttributeToFilter( 'bestseller' , 1 )
            ->setStoreId( $_storeID );

        return $_collections;
    }
    
    protected function _getProductCollection()
    {
        if(is_null($this->_productCollection)){
        $_collections = Mage::getModel( 'catalog/product' )
            ->getCollection()
            ->addAttributeToSelect(Mage::getSingleton("catalog/config")->getProductAttributes())
            ->addAttributeToFilter( 'bestseller' , 1 );
            
               $this->_productCollection = $collection;
        }
        }
        return $this->_productCollection;
    }
}