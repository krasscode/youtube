<?php

class Cplus_Test_Model_Observer extends Mage_Core_Model_Abstract{


    public function addLink(Varien_Event_Observer $observer)
    {
        $menu = $observer->getMenu();
        $tree = $menu->getTree();
        $node = new Varien_Data_Tree_Node(array(
            'name'   => 'Bestseller',
            'id'     => 'bestseller',
            'url'    => Mage::getUrl( 'bestseller' ),
        ), 'id', $tree, $menu);
        $menu->addChild($node);

    }
}

