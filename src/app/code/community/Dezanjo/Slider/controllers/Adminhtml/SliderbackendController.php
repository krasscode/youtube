<?php
class Dezanjo_Slider_Adminhtml_SliderbackendController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Slider"));
        $block = $this->getLayout()->createBlock(
            'Mage_Core_Block_Template',
            'youtube',
            array('template' => 'slider/content.phtml')
        );

        $this->getLayout()->getBlock('content')->append($block);
	   $this->renderLayout();

    }
}